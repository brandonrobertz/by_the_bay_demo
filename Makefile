
deploy:
	npm run build
	rsync -e 'ssh -p 22220' \
		--delete -avC ./build/ \
		brando@bxroberts.org:/var/www/bxroberts.org/public_html/by_the_bay_demo/ \
		--exclude='*.cache' \
		--exclude='node_modules' \
		--exclude='*.map'
