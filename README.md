# By the Bay Text Annotation Demo

This is a very primitive text annotation w/ comments
demo. A live version can be found here:

https://bxroberts.org/by_the_bay_demo/

For local development and testing simply run:

        npm run start

React code is found in `src/` with the main
component being `Annotatable`.
