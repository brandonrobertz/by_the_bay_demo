import React, { Component } from 'react';

/**
 * AddComment is a simple modal for entering and saving/
 * cancelling the addition of a component. This just operates
 * on callbacks, which return the entered text on success.
 *
 * The following callbacks are handled: onAdd, onCancel
 */
class AddComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      display: props.display,
    };
    this.handleAdd = this.handleAdd.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /**
   * Callback for when the add button is clicked.
   * Will call the props.onAdd callback if provided
   * and if the comment entered is not blank.
   */
  handleAdd() {
    if (this.props.onAdd && this.state.comment)
      this.props.onAdd(this.state.comment);

    this.setState({
      display: false,
      comment: '',
    });
  }

  /**
   * Callback for when modal is cancelled.
   * Will call props.onCancel if provided.
   */
  handleCancel() {
    if (this.props.onCancel)
      this.props.onCancel();

    this.setState({
      display: false,
      comment: '',
    });
  }

  handleChange(event) {
    this.setState({comment: event.target.value});
  }

  componentDidMount() {
    // NOTE: we can't focus here because in safari iOS
    // we need to keep continuity on the selection after
    // this modal has been rendered as displayed.
    // this.textarea.focus();
  }

  componentWillReceiveProps(props) {
    this.setState({
      display: props.display
    });
  }

  render() {
    return (
      <div id={'add-comment'} className={this.state.display ? 'show' : 'hide'}>
        <div>
          <textarea value={this.state.comment}
            onChange={this.handleChange}
            ref={textarea => this.textarea = textarea}
            placeholder="Enter comment here...">
          </textarea>
        </div>
        <div>
          <button onClick={this.handleAdd} >Add</button>
          <button onClick={this.handleCancel} >Cancel</button>
        </div>
      </div>
    );
  }
}

export default AddComment;
