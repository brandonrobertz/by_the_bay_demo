import React, { Component } from 'react';

import Annotatable from './Annotatable';
import { data } from './data';

import './main.css';

class App extends Component {
  render() {
    return (
      <div id="main">
        <header className="header">
          <h1 className="title">BTB Text Annotation Demo</h1>
        </header>
        <Annotatable data={data}>
        </Annotatable>
      </div>
    );
  }
}

export default App;
