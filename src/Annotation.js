import React, { Component } from 'react';

/**
 * A simple annotation display component. Shows the
 * annotation symbol and then on click/enter display
 * the annotated quote and the comment. Requires you
 * provide the annotation as props.annotation.
 */
class Annotation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      annotation: props.annotation,
      display: false,
    };
    this.showAnnotation = this.showAnnotation.bind(this);
    this.hideAnnotation = this.hideAnnotation.bind(this);
  }

  showAnnotation() {
    this.setState({
      display: true,
    });
  }

  hideAnnotation() {
    this.setState({
      display: false,
    });
  }

  render() {
    return (
      <span className='annotation-wrapper'>
        <sup className={this.props.className}
          onMouseOver={this.showAnnotation}
          onMouseOut={this.hideAnnotation}>§</sup>
        <span className={this.state.display ? 'show' : 'hide'}>
          <pre className={'quote'}>
            { this.state.annotation.text }
            <sup className={'start-mark'}>§</sup>
          </pre>
          <div className={'comment'}>
            <sup className={'start-mark'}>§</sup>
            { this.state.annotation.comment }
          </div >
        </span>
      </span>
    );
  }
}

export default Annotation;
