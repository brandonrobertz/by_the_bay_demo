import React, { Component } from 'react';
// TODO: extract just required parts to reduce payload size
import _ from 'lodash';
import AddComment from './AddComment';
import Annotation from './Annotation';

class Annotatable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: props.data,
      // store the actual annotations
      annotations: {},
      // control comment modal display
      displayModal: false,
      // store our annotaion object before popping modal
      pendingAnnotation: null,
    };

    // method bindings
    this.onSelection = this.onSelection.bind(this);
    this.renderText = this.renderText.bind(this);
    this.handleAddAnnotation = this.handleAddAnnotation.bind(this);
    this.handleCancelAnnotation = this.handleCancelAnnotation.bind(this);
  }

  /**
   * Get a unique key for a end node offset
   */
  annotationKey(annotation) {
    return `${annotation.endElement}`;
  }

  /**
   * Selections can be drawn in two directions, right to left
   * and left to right. This will normalize the selection to
   * be in terms of left-to-right start/end. On invalid selection,
   * return null. This needs to be checked for.
   *
   * @return {Object}
   */
  normalizeAnnotation(sel) {
    // determine ordering of selection. since it can be done
    // both ways (right to left, l/2/r) we can't just set anchor
    // and focus as start, end w/o checking this
    const relPos = sel.anchorNode.compareDocumentPosition(sel.focusNode);
    let start = null;
    let startOffset = null;
    let end = null;
    let endOffset = null;

    console.log('relative position', relPos);

    // left to right
    if (relPos === 4) {
      console.info('Left to right selection');
      start = sel.anchorNode;
      startOffset = sel.anchorOffset;
      end = sel.focusNode;
      endOffset = sel.focusOffset;
    }
    // same node, use the offset to determine
    else if (relPos === 0) {
      if (sel.anchorOffset < sel.focusOffset) {
        start = sel.anchorNode;
        end = sel.focusNode;
        [startOffset, endOffset] = [sel.anchorOffset, sel.focusOffset];
      } else {
        start = sel.focusNode;
        end = sel.anchorNode;
        [startOffset, endOffset] = [sel.focusOffset, sel.anchorOffset];
      }
    }
    // right to left
    else if (relPos === 2) {
      console.info('Right to left selection');
      start = sel.focusNode;
      startOffset = sel.focusOffset;
      end = sel.anchorNode;
      endOffset = sel.anchorOffset;
    }
    // this shouldn't ever happen, ignore if it does
    else {
      console.warn('Unknown selection', relPos);
      return;
    }

    if ((start.parentElement.id === end.parentElement.id) &&
      (startOffset > endOffset)) {
      [startOffset, endOffset] = [endOffset, startOffset];
      [start, end] = [end, start];
    }

    const annotation = {
      startElement: start.parentElement.id,
      startOffset: startOffset,
      endElement: end.parentElement.id,
      endOffset: endOffset,
      text: sel.toString(),
      comment: null,
    };
    return annotation;
  }

  /**
   * Add an annotation to the state. This
   * assumes all checks, etc have been performed.
   */
  addAnnotation(annotation, comment) {
    console.log('addAnnotation', annotation, comment);
    const commentedAnnotation = Object.assign(
      {}, annotation, { "comment": comment});
    const annotationKey = this.annotationKey(commentedAnnotation);
    // create copy of annotations state (don't mutate)
    const oldOffsetAnnotations = this.state.annotations[annotationKey] || [];
    const newAnnotationByNode = _.sortBy(
      [...oldOffsetAnnotations, commentedAnnotation], a => a.key);
    const newAnnotations = Object.assign(
      {}, this.state.annotations, {
        [annotationKey]: newAnnotationByNode
      });

    this.setState({
      annotations: newAnnotations
    });
  }

  /**
   * Test if an annotation's selection is empty (generated upon click).
   * Note that this depends not only on offsets, but also node names.
   *
   * @return {Boolean}
   */
  selectionEmpty(sel) {
    // handle strange case of clicking while in devtab
    if(!sel || !sel.anchorNode || !sel.focusNode) return true;
    // if we have different node names, we know we have something selected
    if (sel.anchorNode.parentElement.id === sel.focusNode.parentElement.id)
      return (sel.anchorOffset - sel.focusOffset) === 0;
    return sel.toString().length === 0;
  }

  /**
   * Main handler for text selection. Check our
   * selection to make sure it's a valid selection
   * save the pending annotation state and then pop the commentary modal.
   */
  onSelection() {
    let sel = null;
    // all browsers, except IE before version 9
    if (window.getSelection) {
      sel = window.getSelection();
      console.log('Selection', sel);
   }
    // Internet Explorer < 9, not implemented
    else if (document.selection.createRange) {
      sel = document.selection.createRange();
      // TODO: handle older IE
      console.error('Old browsers not yet implemented.', sel);
    }
    else {
      return console.error('No window selection found');
    }

    if (!sel) {
      console.warn('Skipping empty selection', sel);
      return;
    }

    // a click will trigger a zero-length selection
    if (this.selectionEmpty(sel)) {
      console.warn('Skipping blank annotation selection');
      return;
    }

    const annotation = this.normalizeAnnotation(sel);
    if (!annotation) {
      console.warn('Skipping invalid selection');
      return;
    }

    console.log('storing pending annotation', annotation);
    // NOTE: clearning the selection here makes it so in safari iOS
    // the native selector mode never takes over. Not clearing
    // it here lets users enable the selection mode (which selects
    // a single word) then change the selection, each change will
    // trigger the AddComment component which will get the latest
    // annotation bounds.
    // this.clearSelection();
    this.setState({
      pendingAnnotation: annotation,
      displayModal: true,
    });
  }

  clearSelection() {
    if (window.getSelection) {
      if (window.getSelection().empty) {  // Chrome
        window.getSelection().empty();
      } else if (window.getSelection().removeAllRanges) {  // Firefox
        window.getSelection().removeAllRanges();
      }
    } else if (document.selection) {  // IE?
      document.selection.empty();
    }
  }


  /**
   * Callback for when we get a successful comment.
   */
  handleAddAnnotation(comment) {
    console.log('got comment', comment);
    console.log('annotation', this.state.pendingAnnotation);
    this.addAnnotation(this.state.pendingAnnotation, comment);
    this.clearSelection();
    this.setState({
      pendingAnnotation: null,
      displayModal: false,
    });
  }

  handleCancelAnnotation() {
    this.clearSelection();
    this.setState({
      pendingAnnotation: null,
      displayModal: false,
    });
  }

  /**
   * Take our large piece of text and parse
   * it into a react-based representation. This
   * also handles dropping in annotations.
   *
   * @return {Array<ReactElement>}
   */
  renderText() {
    let children = _.split(this.props.data, '\n').map((line, i) => {
      // parent text props
      let props = {
        key: `text-${i}`,
        id: `text-${i}`,
        className: 'text',
      };

      // populate this for node contents
      let children = [line];

      // parse the line for li/span and clean text
      let elType = 'span';
      if (_.indexOf(line, '*') === 0) {
        elType = 'li';
        children[0] = line.slice(1).trim();
      }

      // run over all annotations and insert annotation
      // element to each chunk of relevant text
      // NOTE: this is somewhat inefficient, a double
      // reverse index -> endID -> endOffset -> annotation
      // would be better here
      _.each(this.state.annotations[props.key], (a, ii) => {
        console.log('annotation', a, ii);
        let [text, ...rest] = children;
        let newStartText = text.slice(0, a.endOffset);
        let annoProps = {
          key: `sup-anno-${ii}`,
          id: this.key,
          className: 'annotation',
          annotation: a,
        };
        let annoSymbol = React.createElement(Annotation, annoProps);
        let endText = text.slice(a.endOffset);
        children = [newStartText, annoSymbol, endText, ...rest];
      });

      // this is purely for cheap style
      if (elType === 'span') {
        let id = `br-${i}`;
        let br = <br key={id}/>;
        children.push(br);
      }

      // drop the add annotation modal near end of where the
      // annotation symbol will be added
      if (this.state.pendingAnnotation && props.key === this.state.pendingAnnotation.endElement) {
        let annoModalKey = `annotation-modal-${i}`;
        children.push(<AddComment
          key={annoModalKey}
          display={this.state.displayModal}
          onAdd={this.handleAddAnnotation}
          onCancel={this.handleCancelAnnotation}>
        </AddComment>);
      }

      return React.createElement(elType, props, children);
    });
    // return elements;
    const key = 'text-wrapper';
    return React.createElement('div', {key: key, id: key, className: 'text'}, children);
  }

  render() {
    return (
      <div>
        <div
          onMouseUp={ this.onSelection } onTouchEnd={ this.onSelection }>
          { this.renderText() }
        </div>
      </div>
   );
  }
}

export default Annotatable;
