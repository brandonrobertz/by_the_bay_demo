export const data = `
From San Francisco in a separate formation came the Gypsy Jokers,
three dozen in all, the number-two outlaw club in California, starved
for publicity, and with only one chapter, the Jokers could still look
down on such as the Presidents, Road Rats, Nightriders and Question
Marks, also from the Bay Area, Gomorrah ... with Sodom five hundred
miles to the south in the vast mad bowl of Los Angeles, home turf of
the Satan's Slaves, number three in the outlaw hierarchy, custom-bike
specialists with a taste for the flesh of young dogs, flashy headbands
and tender young blondes with lobotomy eyes; the Slaves were the class
of Los Angeles, and their women clung tight to the leather backs of
these dog-eating, crotch-busting fools as they headed north for their
annual party with the Hell's Angels, who even then viewed the "L.A.
bunch" with friendly condescension ...  which the Slaves didn't mind,
for they could dump with impunity on the other southern clubs -- the
Coffin Cheaters, Iron Horsemen, Galloping Gooses, Comancheros, Stray
Satans and a homeless fringe element of human chancres so foul that
not even the outlaw clubs -- north or south -- would claim them except
in a fight when an extra chain or beer bottle might make the crucial
difference.

The emblem of the Hell's Angels, termed "colors," consists of an
embroidered patch of a winged skull wearing a motorcycle helmet. Just
below the wing of the emblem are the letters "MC." Over this is a band
bearing the words "Hell's Angels." Below the emblem is another patch
bearing the local chapter name, which is usually an abbreviation for
the city or locality. These patches are sewn on the back of a usually
sleeveless denim jacket.

In addition, members have been observed wearing various types of
Luftwaffe insignia and reproductions of German Iron Crosses. Many
affect beards and their hair is usually long and unkempt. Some wear a
single earring in a pierced ear lobe. Frequently they have been
observed to wear belts made of a length of polished motorcycle drive
chain which can be unhooked and used as a flexible bludgeon.

The Hell's Angels seem to have a preference for large heavy-duty
American-made motorcycles[Harley-Davidsons]. Club members usually use
a nickname, designated as their "legal" name, and are carried on club
rolls under that name. Some clubs provide that initiates shall be
tattooed, the cost of which is included in the initiation fee.
Probably the most universal common denominator in identification of
Hell's Angels is their generally filthy condition.  Investigating
officers consistently report these people, both club members and their
female associates, seem badly in need of a bath. Fingerprints are a
very effective means of identification because a high percentage of
Hell's Angels have criminal records ...

* list item 1
* list item 2
* list item 3

Some members of the Hell's Angels as well as members of other
"disreputable" motorcycle clubs belong to what is alleged to be an
elite group termed "One Percenters," which meets monthly at various
places in California. The local Hell's Angels clubs usually meet
weekly ... Requirements for membership or authority to wear the
"1%-er" badge are unknown at this time ... Another patch worn by some
members bears the number "13." It is reported to represent the
thirteenth letter of the alphabet, "M," which in turn stands for
marijuana and indicates the wearer thereof is a user of the drug.

This compact description of rancid, criminal sleaziness is
substantially correct except for the hocus-pocus about the one
percenters. All Angels wear this patch, as do most other outlaws, and
all it means is that they are proud to be part of the alleged one
percent of bike riders whom the American Motorcycle Association
refuses to claim. The AMA is the sporting arm of the Motorcycle,
Scooter and Allied Trades Association, a fastgrowing motorcycle lobby
that is seeking desperately to establish a respectable image -- an
image the Hell's Angels have consistently queered. "We condemn them,"
says an AMA director. "They'd be condemned if they rode horses, mules,
surfboards, bicycles or skateboards. Regretfully, they picked
motorcycles."

The AMA claims to speak for all decent motorcyclists, yet its fifty
thousand or so members rode less than five percent of the 1,500,000
motorcycles registered in the United States in 1965. As one of the
trade magazines noted, that left a lot of outlaws unaccounted for.
`
